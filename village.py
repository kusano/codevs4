# coding: utf-8
# 資源上に村を作るかどうかの基準を決める

import sys

dist = int(sys.argv[1])
stock = int(sys.argv[2])
income = int(sys.argv[3])

w0, w1 = 0, 0
m0, m1 = stock, stock
t0 = []
v1 = False

for i in range(64):
	for j in range(len(t0)):
		t0[j] -= 1
	if len(t0)>0 and t0[0]==0:
		w0 += 1
		del t0[0]

	if w0+len(t0)<5 and m0>=40:
		m0 -= 40
		t0 += [dist]

	if w1<5 and m1>=40 and v1:
		m1 -= 40
		w1 += 1

	if not v1 and m1>=100:
		m1 -= 100
		v1 = True
	
	m0 += income + w0
	m1 += income + w1

	print i, m0, m1
