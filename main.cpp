#include <iostream>
#include <string>

#include "randomai.h"
#include "fsetterai.h"

int main(int argc, char *argv[])
{
    //RandomAI ai;
    FSetterAI ai;

    cout<<"kusano"<<endl;

    while (true)
    {
        int time, stage, turn, res;
        cin>>time>>stage>>turn>>res;
        int selfnum;
        cin>>selfnum;
        vector<Unit> self(selfnum);
        for (Unit &u : self)
            cin>>u.id>>u.y>>u.x>>u.hp>>u.type;
        int enemynum;
        cin>>enemynum;
        vector<Unit> enemy(enemynum);
        for (Unit &u : enemy)
            cin>>u.id>>u.y>>u.x>>u.hp>>u.type;
        int srcnum;
        cin>>srcnum;
        vector<Pos> src(srcnum);
        for (Pos &s: src)
            cin>>s.y>>s.x;
        string end;
        cin>>end;
        if (end!="END")
            return -1;

        int p;
        for (Unit u: self)
        if (u.type == CASTLE)
            p = u.x+u.y<W ? 1 : 2;

        if (p==2)
        {
            for (Unit &s: self)
                s.x = W - s.x - 1,
                s.y = W - s.y - 1;
            for (Unit &e: enemy)
                e.x = W - e.x - 1,
                e.y = W - e.y - 1;
            for (Pos &r: src)
                r.x = W - r.x - 1,
                r.y = W - r.y - 1;
        }

        vector<Cmd> cmd = ai.think(time, stage, turn, res, self, enemy, src);

        if (p==2)
        {
            for (Cmd &c: cmd)
            switch (c.cmd)
            {
            case 'U': c.cmd='D'; break;
            case 'D': c.cmd='U'; break;
            case 'L': c.cmd='R'; break;
            case 'R': c.cmd='L'; break;
            }
        }

        cout<<cmd.size()<<endl;
        for (Cmd &c : cmd)
            cout<<c.id<<" "<<c.cmd<<endl;
    }
}
