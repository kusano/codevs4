#pragma once

#include "ai.h"
#include <random>

class RandomAI : public AI
{
    mt19937 rand;

public:
    vector<Cmd> think(int time, int stage, int turn, int res,
        vector<Unit> self, vector<Unit> enemy, vector<Pos> src)
    {
        vector<Cmd> cmd;

        for (Unit &u : self)
        {
            Cmd c;
            c.id = u.id;
            switch (u.type)
            {
            case WORKER:
                c.cmd = "UDLR"[rand()%4];
                break;
            case KNIGHT:
            case FIGHTER:
            case ASSASSIN:
                c.cmd = "UDLR"[rand()%4];
                break;
            case CASTLE:
                c.cmd = '0';
                break;
            case VILLAGE:
                c.cmd = '0';
                break;
            case STRONGHOLD:
                c.cmd = "123"[rand()%3];
                break;
            }
            cmd.push_back(c);
        }

        return cmd;
    }
};