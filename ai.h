#pragma once

#include <vector>

using namespace std;

struct Pos
{
    int x, y;
    Pos() {};
    Pos(int x, int y) : x(x), y(y) {}
    bool operator==(const Pos &p) const { return x==p.x && y==p.y; }
    bool operator!=(const Pos &p) const { return !(*this==p); }
    bool operator<(const Pos &p) const { return x<p.x || x==p.x && y<p.y; }
    Pos operator+(const Pos &p) const { return Pos(x+p.x, y+p.y); }
};

struct Unit: Pos
{
    int id;
    int hp;
    int type;
};

enum Type
{
    WORKER,
    KNIGHT,
    FIGHTER,
    ASSASSIN,
    CASTLE,
    VILLAGE,
    STRONGHOLD,
    COMBATANT,  //  KNIGHT + FIGHTER + ASSASSIN
};

struct Cmd
{
    int id;
    char cmd;
    Cmd() {}
    Cmd(int id, char cmd) : id(id), cmd(cmd) {}
};

struct Spec
{
    int hp;
    int attack;
    int sight;
    int res;
    char cmd;       //  生成するコマンド
};

Spec spec[] = {
    { 2000,  2,  4,  40, '0'},  //  ワーカー
    { 5000,  2,  4,  20, '1'},  //  ナイト
    { 5000,  2,  4,  40, '2'},  //  ファイター
    { 5000,  2,  4,  60, '3'},  //  アサシン
    {50000, 10, 10,   0, '4'},  //  城
    {20000,  2, 10, 100, '5'},  //  村
    {20000,  2,  4, 500, '6'},  //  拠点
    { 5000,  2,  4,  60, '?'},  //  戦闘ユニット
};

int damage[7][7] = {
    { 100,  100,  100,  100,  100,  100,  100},
    { 100,  500,  200,  200,  200,  200,  200},
    { 500, 1600,  500,  200,  200,  200,  200},
    {1000,  500, 1000,  500,  200,  200,  200},
    { 100,  100,  100,  100,  100,  100,  100},
    { 100,  100,  100,  100,  100,  100,  100},
    { 100,  100,  100,  100,  100,  100,  100},
};

const int W = 100;
const int W2 = 128;
const int CastleArea = 40;

class AI
{
public:
    virtual vector<Cmd> think(int time, int stage, int turn, int res,
        vector<Unit> self, vector<Unit> enemy, vector<Pos> src) = 0;

};
