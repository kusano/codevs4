#pragma once

#include "ai.h"
#include <random>
#include <map>
#include <algorithm>
#include <set>
#include <cassert>
#include <queue>

class FSetterAI : public AI
{
    mt19937 rand;

    //  発見済み資源
    struct Src: Pos
    {
        vector<int> worker;     //  担当ワーカー
        vector<int> comb;       //  戦闘ユニット
        int occupy;             //  資源上のワーカー数
        int village;            //  村
        int total;              //  これまでに送ったワーカー数
        int total_comb;         //  これまでに送った戦闘ユニット数

        Src() {}
        Src(Pos p): Pos(p), village(-1), total(0), total_comb(0) {}
    };

    //  盤面の端
    //  探索でワーカーがここに向かう
    struct Edge: Pos
    {
        int dir;                //  0: x->yと移動　1: y->xと移動
        int worker;             //  担当ワーカー
        bool achieve;           //  端に到達したか

        Edge(int x, int y, int dir):
            Pos(x, y),
            dir(dir),
            worker(-1),
            achieve(false)
        {}
    };

    //  敵の城周辺の画面端
    struct CEdge: Pos
    {
        vector<int> comb;       //  ここに向かう戦闘ユニット
        bool achieve;           //  到達したか

        CEdge(int x, int y): Pos(x,y), achieve(false) {}
    };

    //  スクラム
    struct Scrum: Pos
    {
        int state;              //  0: 準備中　1: 進む
        int size;               //  この人数になったら進む
        vector<int> comb;       //  属する戦闘ユニット

        Scrum(int x, int y, int size): Pos(x,y), state(0), size(size) {}
    };

    vector<Src> src;            //  資源マス
    Unit scastle;               //  味方城
    Unit ecastle;               //  敵城　id=-1ならば未発見
    bool sight[W][W2];          //  一度でも視界に入ったか
    int setter;                 //  拠点作成ワーカー
    vector<Edge> edge;          //  画面端
    vector<CEdge> cedge;        //  敵の城周辺の画面端
    vector<int> strong;         //  拠点
    vector<Scrum> scrum;        //  スクラム
    int scrum_num;              //  これまでの作成したスクラム数
    vector<int> guard;          //  味方の城の防御
    vector<int> guard_comb;     //  味方の城の防御
    vector<Unit> newborn;       //  新たに作成したユニット

    int prev_turn;              //  前ターンのturn
    map<int, Unit> prev_self;   //  前ターンのself
    Unit prev_ecastle;          //  前ターンのecastle

    //  マンハッタン距離
    int dist(Pos a, Pos b)
    {
        return abs(a.x-b.x) + abs(a.y-b.y);
    }

    //  fromからtoに向かう動きを求める
    char head(Pos from, Pos to)
    {
        int u = max(from.y - to.y, 0);
        int d = max(to.y - from.y, 0);
        int l = max(from.x - to.x, 0);
        int r = max(to.x - from.x, 0);

        if (u+d+l+r == 0)
            return ' ';

        int p = rand()%(u+d+l+r);

        if (p<u) return 'U'; p -= u;
        if (p<d) return 'D'; p -= d;
        if (p<l) return 'L'; p -= l;
        if (p<r) return 'R'; p -= r;

        assert(p == 0);
        return ' ';
    }

    //  ステージごとの初期化
    void init(vector<Unit> &self)
    {
        src.clear();

        scastle.id = -1;
        ecastle.id = -1;
        ecastle.x = W - 1;
        ecastle.y = W - 1;

        for (int y=0; y<W; y++)
        for (int x=0; x<W; x++)
            sight[y][x] = false;

        setter = -1;

        //  画面端
        //  ↑　↑→→→→
        //  ←←城
        //  ↓　↓→→→→
        //  ↓　↓
        //  ↓　↓→→→→
        Unit castle;
        for (Unit s: self)
            if (s.type == CASTLE)
                castle = s;
        edge.clear();
        int r = spec[WORKER].sight;
        if (castle.x < castle.y)
        {
            for (int y=r; y-r<W; y+=2*r+1)
                edge.push_back(Edge(W-1, min(y,W-1), 1));
            for (int x=castle.x-2*r-1; x+r>=0; x-=2*r+1)
                edge.push_back(Edge(max(x,0), 0, 0)),
                edge.push_back(Edge(max(x,0), W-1, 0));
        }
        else
        {
            for (int x=r; x-r<W; x+=2*r+1)
                edge.push_back(Edge(min(x,W-1), W-1, 0));
            for (int y=castle.y-2*r-1; y+r>=0; y-=2*r+1)
                edge.push_back(Edge(0, max(y,0), 1)),
                edge.push_back(Edge(W-1, max(y,0), 1));
        }

        //  敵の城周辺の画面端
        cedge.clear();
        cedge.push_back(CEdge(W-1,W-1));
        for (int p=W-1; p-r>W-CastleArea;)
        {
            p-=2*r+1;
            cedge.push_back(CEdge(W-1,p));
            cedge.push_back(CEdge(p,W-1));
        }

        strong.clear();
        scrum.clear();
        scrum_num = 0;
        guard.clear();
        guard_comb.clear();
        newborn.clear();
        prev_self.clear();
        prev_ecastle.id = -1;

        //  初期のワーカーの割り振り
        //  1人はsetter、あとはEdge
        for (Unit u: self)
        if (u.type == WORKER)
        {
            if (setter == -1)
                setter = u.id;
            else
                for (Edge &e: edge)
                if (e.worker == -1 )
                {
                    e.worker = u.id;
                    break;
                }
        }
    }

    //  ユニットの作成
    //  このメッソドが返したIDは次のターンで実際のIDに置換される
    int buildUnit(Unit parent, int type, int &res)
    {
        //cerr<<parent.x<<" "<<parent.y<<" "<<type<<" "<<res<<endl;

        int id = -(int)newborn.size() - 2;

        Unit u;
        u.x = parent.x;
        u.y = parent.y;
        u.type = type;
        newborn.push_back(u);

        res -= spec[type].res;

        return id;
    }

    //  新規ユニットに付けた仮IDを実際のIDに
    //  死んだ場合は-1に
    int replaceId(int id, map<int, Unit> &self, set<int> &used)
    {
        if (id <= -2)
        {
            //  新規ユニットのID
            Unit n = newborn[-(id+2)];
            for (auto s: self)
            if (s.second == n &&
                s.second.type == n.type &&
                used.count(s.second.id) == 0)
            {
                used.insert(s.second.id);
                return s.second.id;
            }

            //  生成直後に死んだ
            return -1;
        }
        else if (id == -1)
        {
            //  無効を示すID
            return -1;
        }
        else
        {
            //  通常のID
            return self.count(id)>0 ? id : -1;
        }
    }

    //  各要素にreplaceIdを適用し、-1になったものを削除
    vector<int> replaceIdList(vector<int> &v, map<int, Unit> &self,
        set<int> &used)
    {
        vector<int> r;
        for (int a: v)
        {
            a = replaceId(a, self, used);
            if (a>=0)
                r.push_back(a);
        }
        return r;
    }

    //  セッター
    void moveSetter(map<int, char> &cmd, map<int, Unit> &self, map<int, Unit> &enemy)
    {
        if (setter == -1)
            return;

        //  敵を避けつつ右下へ向かう
        Unit u = self[setter];

        int D[W][W2];
        for (int y=0; y<W; y++)
        for (int x=0; x<W; x++)
            D[y][x] = u.hp>=spec[u.type].hp/2 ? 1000 : 10;
        for (auto &ei: enemy)
        {
            Unit e = ei.second;
            int r = spec[e.type].attack + 1;
            for (int y=e.y-r; y<=e.y+r; y++)
            for (int x=e.x-r; x<=e.x+r; x++)
                if (0<=x && x<W && 0<=y && y<W && dist(Pos(x,y), e)<=r)
                    D[y][x] += damage[e.type][u.type];
        }

        int T[W][W2];
        bool F[W][W2];
        for (int y=0; y<W; y++)
        for (int x=0; x<W; x++)
            T[y][x] = 99999999,
            F[y][x] = false;

        Pos dir[5] =
        {
            Pos(0,1),
            Pos(1,0),
            Pos(0,-1),
            Pos(-1,0),
            Pos(0,0),
        };
        char dirc[] = "DRUL ";

        //  現在地が右上なら下に、左下なら右に
        if (u.x < u.y)
        {
            swap(dir[0], dir[1]);
            swap(dir[2], dir[3]);
            swap(dirc[0], dirc[1]);
            swap(dirc[2], dirc[3]);
        }

        priority_queue<pair<int,Pos>> Q;
        T[W-1][W-1] = 0;
        Q.push(make_pair(0,Pos(W-1,W-1)));
        while (!Q.empty())
        {
            Pos p = Q.top().second;
            Q.pop();
            if (!F[p.y][p.x])
            {
                F[p.y][p.x] = true;
                for (int d=0; d<5; d++)
                if (dir[d] != Pos(0,0))
                {
                    Pos q = p + dir[d];
                    if (0<=q.x && q.x<W && 0<=q.y && q.y<W)
                    {
                        int t = T[p.y][p.x] + D[q.y][q.x];
                        if (t < T[q.y][q.x])
                        {
                            T[q.y][q.x] = t;
                            Q.push(make_pair(-t, q));
                        }
                    }
                }
            }
        }

        int m = 99999999;
        int c;
        for (int d=0; d<5; d++)
        {
            int x = u.x+dir[d].x;
            int y = u.y+dir[d].y;
            if (0<=x && x<W && 0<=y && y<W && T[y][x] < m)
                m = T[y][x],
                c = dirc[d];
        }
        cmd[u.id] = c;
    }

    //  探索
    void moveSearcher(map<int, char> &cmd, map<int, Unit> &self)
    {
        vector<bool> used(src.size());

        for (Edge &e: edge)
        if (e.worker >= 0)
        {
            Unit &u = self[e.worker];

            if (u == e)
                e.achieve = true;

            //  視界内にワーカーが5人以下の資源があれば
            //  最も近い資源に向かう
            int d = 9999;
            int r = -1;
            for (int i=0; i<(int)src.size(); i++)
            if (!used[i] &&
                (src[i].occupy - (src[i]==u?1:0)) < 5 &&
                dist(u, src[i]) < d)
            {
                d = dist(u, src[i]);
                r = i;
            }
            if (d <= spec[u.type].sight)
            {
                used[r] = true;
                cmd[u.id] = head(u, src[r]);
            }
            else if (!e.achieve)
            {
                //  担当するエッジへ向かう
                if (e.dir==0 && u.x!=e.x)
                    cmd[u.id] = u.x<e.x ? 'R' : 'L';
                else if (e.dir==1 && u.y!=e.y)
                    cmd[u.id] = u.y<e.y ? 'D' : 'U';
                else
                    cmd[u.id] = head(u, e);
            }
            else
            {
                //  敵の城に向かう
                if (ecastle.id >= 0)
                    cmd[u.id] = head(u, ecastle);
                else
                    if (e.dir==0)
                        cmd[u.id] = head(u, Pos(W-1, W-spec[u.type].sight*2));
                    else
                        cmd[u.id] = head(u, Pos(W-spec[u.type].sight*2, W-1));
            }
        }
    }

    //  資源採掘ワーカー
    void moveMiner(map<int, char> &cmd, map<int, Unit> &self)
    {
        for (Src s: src)
        for (int w: s.worker)
            cmd[w] = head(self[w], s);
    }

    //  資源用戦闘ユニット
    void moveSrcComb(map<int, char> &cmd, map<int, Unit> &self)
    {
        for (Src s: src)
        for (int c: s.comb)
            cmd[c] = head(self[c], s);
    }

    //  敵の城探索戦闘ユニット
    void moveCSearcher(map<int, char> &cmd, map<int, Unit> &self)
    {
        for (CEdge &e: cedge)
        for (int c: e.comb)
        {
            if (self[c] == e)
                e.achieve = true;

            if (!e.achieve)
                cmd[c] = head(self[c], e);
            else
                cmd[c] = head(self[c], ecastle);
        }
    }

    //  スクラムの移動
    void moveScrum(map<int, char> &cmd, map<int, Unit> &self)
    {
        const Pos delta[] = {
            Pos(0,0),
            Pos(1,0),
            Pos(0,1),
            Pos(-1,0),
            Pos(0,-1),
        };

        for (Scrum &s: scrum)
        {
            if (s.state == 0)
            {
                for (int c=0; c<(int)s.comb.size(); c++)
                    cmd[s.comb[c]] = head(self[s.comb[c]], s+delta[c/10]);
            }
            else
            {
                int d;
                int m = 9999;
                for (int i=0; i<4; i++)
                {
                    Pos p = ecastle+delta[i+1]+delta[i+1];
                    if (0<=p.x && p.x<W && 0<=p.y && p.y<W &&
                        dist(p, s) < m)
                        m = dist(p, s),
                        d = i;
                }

                switch (head(s, ecastle+delta[d+1]+delta[d+1]))
                {
                case 'U': s.y--; break;
                case 'D': s.y++; break;
                case 'L': s.x--; break;
                case 'R': s.x++; break;
                }

                for (int c=0; c<(int)s.comb.size(); c++)
                    cmd[s.comb[c]] = head(self[s.comb[c]], s+delta[c/10]);
            }
        }
    }

    //  城防御用ワーカー・戦闘ユニット
    void moveGuard(map<int, char> &cmd, map<int, Unit> &self)
    {
        const Pos delta[] = {
            Pos(0,0),
            Pos(1,0),
            Pos(1,1),
            Pos(0,1),
            Pos(-1,1),
            Pos(-1,0),
            Pos(-1,-1),
            Pos(0,-1),
            Pos(1,-1),
            Pos(0,0),
        };

        int c = 0;
        for (int g: guard_comb)
            cmd[g] = head(self[g], scastle+delta[min((c+1)/10, 9)]),
            c++;
        for (int g: guard)
            cmd[g] = head(self[g], scastle+delta[min((c+1)/10, 9)]),
            c++;
    }

    //  拠点を作成
    void buildStronghold(map<int, char> &cmd, map<int, Unit> &self, int turn, int &res)
    {
        if (res < spec[STRONGHOLD].res)
            return;

        Pos center;
        if (ecastle.id >= 0)
            center = ecastle;
        else
            center = Pos(W-CastleArea/2, W-CastleArea/2);

        int d = 9999;
        int w = -1;

        for (auto u: self)
        if (u.first >= 0 &&
            u.second.type == WORKER)
            if (dist(u.second, center) < d)
                d = dist(u.second, center),
                w = u.first;

        if (w != -1)
        {
            cerr<<"Stronghold: "<<turn<<endl;

            strong.push_back(buildUnit(self[w], STRONGHOLD, res));
            cmd[w] = spec[STRONGHOLD].cmd;
        }
    }

    //  資源攻撃用の戦闘ユニットを生成
    void buildSrcComb(map<int, char> &cmd, map<int, Unit> &self, int &res)
    {
        for (int s: strong)
        if (s >= 0 &&
            res >= spec[COMBATANT].res &&
            (cmd.count(s)==0 || cmd[s]==' '))
        {
            int d = 9999;
            Src *m = NULL;

            for (Src &r: src)
            if (r.comb.empty() &&
                r.occupy == 0)
            {
                if (dist(r, self[s]) < d)
                {
                    d = dist(r, self[s]);
                    m = &r;
                }
            }

            if (m != NULL)
            {
                m->comb.push_back(buildUnit(self[s], ASSASSIN, res));
                m->total_comb++;
                cmd[s] = spec[ASSASSIN].cmd;
            }
        }
    }

    //  敵の城探索用の戦闘ユニットを作成
    void buildCSearcher(map<int, char> &cmd, map<int, Unit> &self, int &res)
    {
        for (int s: strong)
        if (s >= 0 &&
            res >= spec[COMBATANT].res &&
            (cmd.count(s)==0 || cmd[s]==' '))
        {
            //  未到達で戦闘ユニットが最小の箇所を選択
            int m = 1;
            int e = -1;
            for (int i=0; i<(int)cedge.size(); i++)
                if (!cedge[i].achieve &&
                    (int)cedge[i].comb.size() < m)
                    m = (int)cedge[i].comb.size(),
                    e = i;

            if (e >= 0)
            {
                int type = KNIGHT+rand()%3;
                cedge[e].comb.push_back(buildUnit(self[s], type, res));
                cmd[s] = spec[type].cmd;
            }
        }
    }

    //  敵の城攻撃用の戦闘ユニットを作成
    void buildScrum(map<int, char> &cmd, map<int, Unit> &self, int &res)
    {
        //  敵の城に近い順にソート
        vector<pair<int,int>> st;
        for (int s: strong)
        if (s >= 0)
            st.push_back(make_pair(dist(self[s], ecastle), s));
        sort(st.begin(), st.end());

        for (auto si: st)
        if (res >= spec[COMBATANT].res &&
            (cmd.count(si.first)==0 || cmd[si.first]==' '))
        {
            Unit &s = self[si.second];

            int c = -1;
            for (int i=0; i<(int)scrum.size(); i++)
                if (scrum[i]==s && scrum[i].state==0)
                    c = i;
            if (c == -1)
            {
                c = (int)scrum.size();
                scrum.push_back(Scrum(s.x, s.y, min((scrum_num+1)*10, 30)));
                scrum_num++;
            }

            int type = KNIGHT+rand()%3;

            scrum[c].comb.push_back(buildUnit(s, type, res));
            if ((int)scrum[c].comb.size() >= scrum[c].size)
                scrum[c].state = 1;

            cmd[s.id] = spec[type].cmd;
        }
    }

    //  味方の城防衛用の攻撃ユニット
    void buildGuardComb(map<int, char> &cmd, map<int, Unit> &self, int &res)
    {
        for (int s: strong)
        if (s >= 0 &&
            res >= spec[COMBATANT].res &&
            (cmd.count(s)==0 || cmd[s]==' '))
        {
            if (guard_comb.size()<50)
            {
                int type = KNIGHT+rand()%3;
                guard_comb.push_back(buildUnit(self[s], type, res));
                cmd[s] = spec[type].cmd;
            }
        }
    }

    //  資源採掘用ワーカーを作成
    void buildMiner(map<int, char> &cmd, map<int, Unit> &self, int &res)
    {
        vector<pair<int,pair<Unit *,Src *>>> P;
        for (Src &r: src)
        {
            if (cmd.count(scastle.id)==0 || cmd[scastle.id]==' ')
                P.push_back(make_pair(dist(scastle,r), make_pair(&scastle, &r)));
            for (Src &t: src)
            if (t.village >= 0)
                P.push_back(make_pair(dist(t,r), make_pair(&self[t.village], &r)));
        }
        sort(P.begin(), P.end());

        set<int> used;

        for (auto &p: P)
        {
            int d = p.first;
            Unit &u = *p.second.first;
            Src &r = *p.second.second;

            int c = (int)r.worker.size();
            for (Edge &e: edge)
                if (e.worker>=0 && self[e.worker]==r)
                    c++;

            if (res >= spec[WORKER].res &&
                used.count(u.id) == 0 &&
                c < 5 &&
                r.total < 5)
            {
                used.insert(u.id);
                r.worker.push_back(buildUnit(u, WORKER, res));
                r.total++;
                cmd[u.id] = spec[WORKER].cmd;
            }
        }
    }

    //  村作成
    //  資源採掘用ワーカーを作成すべきかを返す
    bool buildVillage(map<int, char> &cmd, map<int, Unit> &self, int &res)
    {
        //  i番目の資源に村を作ったとしてシミュレーションを行い
        //  最終的な資源量の多いところに村を作る

        //  i番目の資源に村を作った時の最終的な資源量
        //  nは村を作らない場合
        int n = (int)src.size();
        vector<int> S(n+1);

        for (int test=0; test<=n; test++)
        {
            if (test < n &&
                (src[test].occupy == 0 ||
                 src[test].village >= 0))
            {
                S[test] = 0;
                continue;
            }

            vector<vector<int>> work(src.size());
            vector<bool> vill(src.size());
            int r = res;

            set<int> used;
            for (int s=0; s<n; s++)
            {
                for (int w: src[s].worker)
                    work[s].push_back(dist(src[s], self[w]));
                vill[s] = src[s].village >= 0;
                for (Edge &e: edge)
                    if (e.worker >= 0 && used.count(e.worker)==0 &&
                        dist(self[e.worker], src[s]) <= spec[WORKER].sight)
                    {
                        used.insert(e.worker);
                        work[s].push_back(dist(self[e.worker], src[s]));
                    }
            }

            for (int turn=0; turn<1000; turn++)
            {
                if (r >= spec[VILLAGE].res &&
                    test<n && !vill[test])
                {
                    r -= spec[VILLAGE].res;
                    vill[test] = true;
                }

                if (r >= spec[WORKER].res &&
                    (test==n || vill[test]))
                {
                    //  建築
                    vector<pair<int,pair<int,int>>> P;
                    for (int s=0; s<n; s++)
                    if (work[s].size() < 5)
                    {
                        P.push_back(make_pair(dist(scastle,src[s]), make_pair((int)src.size(),s)));
                        for (int t=0; t<n; t++)
                        if (vill[t])
                            P.push_back(make_pair(dist(src[t],src[s]), make_pair(t,s)));
                    }
                    sort(P.begin(), P.end());

                    vector<bool> used(n+1);
                    for (int i=0; i<(int)P.size() && r>=spec[WORKER].res; i++)
                    if (!used[P[i].second.first])
                    {
                        used[P[i].second.first] = true;
                        r -= spec[WORKER].res;
                        work[P[i].second.second].push_back(P[i].first+1);   //  建築直後の移動ができない分
                    }
                }

                //  移動
                for (int s=0; s<n; s++)
                    for (int &w: work[s])
                        w = max(0, w-1);

                //  資源獲得
                r += 10;
                for (int s=0; s<n; s++)
                    for (int &w: work[s])
                        if (w==0)
                            r++;
            }

            S[test] = r;
        }

        int ms = 0;
        int m = 0;
        for (int s=0; s<=n; s++)
            if (S[s] > m)
            {
                m = S[s];
                ms = s;
            }

        if (ms == n)
            return true;

        if (res >= spec[VILLAGE].res)
        {
            for (auto u: self)
            if (u.second.type==WORKER && u.second==src[ms])
            {
                src[ms].village = buildUnit(u.second, VILLAGE, res);
                cmd[u.first] = spec[VILLAGE].cmd;
                return true;
            }
        }

        return false;
    }

    //  資源探索用ワーカー
    void buildSearcher(map<int, char> &cmd, int &res)
    {
        if (res >= spec[WORKER].res &&
            (cmd.count(scastle.id) == 0 || cmd[scastle.id] == ' '))
        {
            //  担当ワーカーがいないエッジがあれば作成
            for (Edge &e: edge)
            if (!e.achieve &&
                e.worker == -1)
            {
                e.worker = buildUnit(scastle, WORKER, res);
                cmd[scastle.id] = spec[WORKER].cmd;
            }
        }
    }

    //  城防御ワーカー
    void buildGuard(map<int, char> &cmd, map<int, Unit> &enemy, int &res)
    {
        if (res >= spec[WORKER].res)
        {
            //  城の視界内に敵と同数の防御用ワーカー生成
            int c = 0;
            for (auto e: enemy)
                if (dist(scastle, e.second) <= spec[scastle.type].sight)
                    c++;
            if ((int)guard.size() < min(c, 49))
            {
                guard.push_back(buildUnit(scastle, WORKER, res));
                cmd[scastle.id] = spec[WORKER].cmd;
            }
        }
    }

public:
    FSetterAI(): prev_turn(-1) {}

    vector<Cmd> think(int time, int stage, int turn, int res,
        vector<Unit> self_, vector<Unit> enemy_, vector<Pos> src_)
    {
        //cerr<<"Turn "<<turn<<endl;

        //  初期化
        if (turn == 0)
        {
            if (prev_turn >= 0)
                cerr<<"Turn: "<<prev_turn<<endl,
                cerr<<endl;
            cerr<<"Stage: "<<stage<<endl;

            init(self_);
        }

        //  入力処理
        map<int, Unit> self;
        for (Unit s: self_)
            self[s.id] = s;
        map<int, Unit> enemy;
        for (Unit e: enemy_)
            enemy[e.id] = e;

        for (auto s: self)
            if (s.second.type == CASTLE)
                scastle = s.second;
        for (auto e: enemy)
            if (e.second.type == CASTLE)
            {
                if (ecastle.id == -1)
                    cerr<<"Castle: "<<turn<<endl;
                ecastle = e.second;
            }

        for (Pos r: src_)
            if (find(src.begin(), src.end(), r) == src.end())
                src.push_back(Src(r));

        //  ID変換
        {
            set<int> used;
            for (auto u: prev_self)
                used.insert(u.first);

            setter = replaceId(setter, self, used);

            for (Src &r: src)
                r.worker = replaceIdList(r.worker, self, used),
                r.comb = replaceIdList(r.comb, self, used),
                r.village = replaceId(r.village, self, used);

            for (Edge &e: edge)
                e.worker = replaceId(e.worker, self, used);

            for (CEdge &e: cedge)
                e.comb = replaceIdList(e.comb, self, used);

            strong = replaceIdList(strong, self, used);

            for (Scrum &s: scrum)
                s.comb = replaceIdList(s.comb, self, used);

            guard = replaceIdList(guard, self, used);

            guard_comb = replaceIdList(guard_comb, self, used);

            newborn.clear();
        }

        //  資源上のワーカーを数える
        for (Src &r: src)
        {
            r.occupy = 0;
            for (int w: r.worker)
                if (self[w] == r)
                    r.occupy++;
            for (Edge &e: edge)
                if (e.worker>=0 && self[e.worker]==r)
                    r.occupy++;
        }

        //  視界の更新
        for (auto &t: self)
        {
            Unit &u = t.second;

            int r = spec[u.type].sight;
            for (int y=u.y-r; y<=u.y+r; y++)
            for (int x=u.x-r; x<=u.x+r; x++)
                if (0<=x && x<W && 0<=y && y<W &&
                    dist(Pos(x,y), u)<=r)
                    sight[y][x] = true;
        }

        //  未発見の城は未開拓地点の平均と仮定する
        if (ecastle.id == -1)
        {
            int cx = 0;
            int cy = 0;
            int cn = 0;

            for (int y=W-CastleArea-1; y<W; y++)
            for (int x=2*W-y-CastleArea-2; x<W; x++)
                if (!sight[y][x])
                    cx += x,
                    cy += y,
                    cn++;
            assert(cn>0);
            ecastle.x = (cx+cn/2)/cn;
            ecastle.y = (cy+cn/2)/cn;
        }

        //  敵の城が見つかったら敵の城探索用のユニットを攻撃用に変更
        if (prev_ecastle.id == -1 && ecastle.id >= 0)
        {
            vector<int> comb;
            for (CEdge &e: cedge)
            {
                comb.insert(comb.end(), e.comb.begin(), e.comb.end());
                e.comb.clear();
            }

            if (!comb.empty())
            {
                vector<int> F[W][W2];
                for (int c: comb)
                    F[self[c].y][self[c].x].push_back(c);

                for (int y=0; y<W; y++)
                for (int x=0; x<W; x++)
                if (!F[y][x].empty())
                {
                    Scrum s(x, y, (int)F[y][x].size());
                    s.state = 1;
                    s.comb = F[y][x];
                    scrum.push_back(s);
                }
            }
        }

        //  行動決定
        map<int, char> cmd;

        //  移動
        moveSetter(cmd, self, enemy);
        moveSearcher(cmd, self);
        moveMiner(cmd, self);
        moveSrcComb(cmd, self);
        moveCSearcher(cmd, self);
        moveScrum(cmd, self);
        moveGuard(cmd, self);

        //  建設
        buildGuard(cmd, enemy, res);
        buildStronghold(cmd, self, turn, res);

        //if (turn >= 150)
        //  buildGuardComb(cmd, self, res);

        if (ecastle.id == -1)
            buildCSearcher(cmd, self, res);
        if (rand()%2==0)
            buildSrcComb(cmd, self, res);
        buildScrum(cmd, self, res);

        //  拠点が無い場合：一定のターン以降拠点作成を優先
        //  拠点がある場合：攻撃ユニットの作成を防がないため資源がアサシン以上の場合のみ
        if ((!strong.empty() || turn<100) &&
            (strong.empty() || res>=spec[COMBATANT].res))
        {
            if (buildVillage(cmd, self, res))
                buildMiner(cmd, self, res);

            buildSearcher(cmd, res);
        }

        //  今回の引数を保存
        prev_turn = turn;
        prev_self = self;
        prev_ecastle = ecastle;

        vector<Cmd> cmdret;
        for (auto c: cmd)
            if (c.second != ' ')
                cmdret.push_back(Cmd(c.first, c.second));
        return cmdret;
    }
};
